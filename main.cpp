#include <iostream>
#include <cassert>
/*====================================================================================================================*/
class CNode
{
private:

    unsigned int id;

    unsigned int revenue;

    int degree;

    CNode* parent;

    CNode* child;

    CNode* sibling;

public:

    CNode                   ( unsigned int id, unsigned int revenue );

    CNode                   ( );

    ~CNode                  ( );

    unsigned int getId      ( ) const;

    void setId              ( unsigned int id );

    unsigned int getRevenue ( ) const;

    void setRevenue         ( unsigned int revenue );

    int compareTo           ( const CNode* y );

    int getDegree           ( ) const;

    void setDegree          ( int degree );

    CNode *getParent        ( ) const;

    void setParent          ( CNode *parent_node );

    CNode *getChild         ( ) const;

    void setChild           ( CNode *child_node );

    CNode *getSibling       ( ) const;

    void setSibling         ( CNode *sibling_node );
};
/*--------------------------------------------------------------------------------------------------------------------*/
CNode::CNode( )
{
    id = revenue = degree = 0;
    parent = child = sibling = nullptr;
}
/*--------------------------------------------------------------------------------------------------------------------*/
CNode::CNode( unsigned int id, unsigned int revenue )
{
    this->id = id;
    this->revenue = revenue;
    degree = 0;
    parent = child = sibling = nullptr;
}
/*--------------------------------------------------------------------------------------------------------------------*/
CNode::~CNode( )
{
    if( child != nullptr )
        delete child;

    if( sibling != nullptr )
        delete sibling;
}
/*--------------------------------------------------------------------------------------------------------------------*/
int CNode::compareTo( const CNode* y )
{
    if( y->revenue < this->revenue )
        return 1;
    else if( y->revenue == this->revenue && y->id < this->id )
        return 1;
    else if( y->revenue == this->revenue && y->id == this->id )
        return 0;
    else
        return -1;
}
/*--------------------------------------------------------------------------------------------------------------------*/
unsigned int CNode::getId( ) const { return id; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CNode::setId( unsigned int id ) { this->id = id; }
/*--------------------------------------------------------------------------------------------------------------------*/
unsigned int CNode::getRevenue( ) const { return revenue; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CNode::setRevenue( unsigned int revenue ) { this->revenue = revenue; }
/*--------------------------------------------------------------------------------------------------------------------*/
int CNode::getDegree( ) const { return degree; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CNode::setDegree( int degree) { this->degree = degree; }
/*--------------------------------------------------------------------------------------------------------------------*/
CNode *CNode::getParent( ) const { return parent; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CNode::setParent( CNode *parent_node ) { this->parent = parent_node; }
/*--------------------------------------------------------------------------------------------------------------------*/
CNode *CNode::getChild( ) const { return child; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CNode::setChild( CNode *child_node ) { this->child = child_node; }
/*--------------------------------------------------------------------------------------------------------------------*/
CNode *CNode::getSibling( ) const { return sibling; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CNode::setSibling( CNode *sibling_node ) { this->sibling = sibling_node; }
/*====================================================================================================================*/
class CChain
{
private:

    CNode* head;

    int chain_id;

    int mh_index;

public:

    CChain             ( CNode* node );

    CChain             ( );

    ~CChain            ( );

    int getId          ( ) const;

    CNode* Union       ( CChain* heap );

    CNode* Merge       ( CChain* heap1, CChain* heap2 );

    bool extractMin    ( unsigned int& id );

    void removeRoot    ( CNode* root, CNode* prev );

    CNode* findMin     ( ) const;

    void Insert        ( unsigned id, unsigned revenue );

    void BinominalLink ( CNode* x, CNode* y );

    void swap          ( CNode *& x, CNode*& y );

    int compareTo      ( const CChain * y );

    int getMhIndex     ( ) const;

    void setMhIndex    ( int mhIndex );

    int getChainId     ( ) const;

    void setChainId    ( int chainId );

    bool isEmpty       ( ) { return head == nullptr; }

};
/*--------------------------------------------------------------------------------------------------------------------*/
CChain::CChain( CNode* node )
{
    head = node;
    chain_id = 0;
    mh_index = 0;
}
/*--------------------------------------------------------------------------------------------------------------------*/
CChain::CChain( )
{
    head = nullptr;
    chain_id = 0;
    mh_index = 0;
}
/*--------------------------------------------------------------------------------------------------------------------*/
CChain::~CChain( )
{
    delete head;
}
/*--------------------------------------------------------------------------------------------------------------------*/
void CChain::swap( CNode *&x, CNode *&y )
{
    CNode* temp = x;
    x = y;
    y = temp;
}
/*--------------------------------------------------------------------------------------------------------------------*/
int CChain::getId( ) const { return chain_id; }
/*--------------------------------------------------------------------------------------------------------------------*/
CNode* CChain::Union( CChain *heap )
{
    CNode* newhead = Merge(this, heap );

    head = nullptr;
    heap->head = nullptr;

    if( newhead == nullptr )
        return nullptr;

    CNode* prev = nullptr;
    CNode* curr = newhead;
    CNode* next = newhead->getSibling();

    while( next != nullptr )
    {
        if( curr->getDegree() != next->getDegree()
         || ( next->getSibling() != nullptr && next->getSibling()->getDegree() == curr->getDegree() ) )
        {
            prev = curr;
            curr = next;
        }
        else
        {
            if( curr->compareTo( next ) < 0 )
            {
                curr->setSibling( next->getSibling() );
                BinominalLink( curr, next );
            }
            else
            {
                if( prev == nullptr )
                    newhead = next;
                else
                    prev->setSibling( next );

                BinominalLink( next, curr );
                curr = next;
            }
        }

        next = curr->getSibling();
    }

    return newhead;
}
/*--------------------------------------------------------------------------------------------------------------------*/
void CChain::Insert( unsigned id, unsigned revenue )
{
    CNode * node = new CNode( id, revenue );
    CChain* tmp = new CChain( node );

    head = Union( tmp );

    delete tmp;
}
/*--------------------------------------------------------------------------------------------------------------------*/
CNode* CChain::Merge( CChain* heap1, CChain* heap2 )
{
    if(heap1->head == nullptr )
        return heap2->head;

    if(heap2->head == nullptr )
        return heap1->head;

    CNode* newhead = nullptr;
    CNode* heap1Next = heap1->head;
    CNode* heap2Next = heap2->head;
    CNode* tail = nullptr;

    if( heap1->head->getDegree() <= heap2->head->getDegree() )
    {
        newhead = heap1->head;
        heap1Next = heap1Next->getSibling();
    }
    else
    {
        newhead = heap2->head;
        heap2Next = heap2Next->getSibling();
    }

    tail = newhead;

    while( heap1Next != nullptr && heap2Next != nullptr )
    {
        if( heap1Next->getDegree() <= heap2Next->getDegree() )
        {
            tail->setSibling( heap1Next );
            heap1Next = heap1Next->getSibling();
        }
        else
        {
            tail->setSibling( heap2Next );
            heap2Next = heap2Next->getSibling();
        }

        tail = tail->getSibling();
    }

    if( heap1Next != nullptr )
        tail->setSibling( heap1Next );
    else
        tail->setSibling( heap2Next );

    return newhead;
}
/*--------------------------------------------------------------------------------------------------------------------*/
CNode* CChain::findMin( ) const
{
    if( head == nullptr )
        return nullptr;

    CNode* min = head;
    CNode* next = min->getSibling();

    while( next != nullptr )
    {
        if( next->compareTo( min ) < 0 )
            min = next;

        next = next->getSibling();
    }

    return min;
}
/*--------------------------------------------------------------------------------------------------------------------*/
bool CChain::extractMin( unsigned int& id )
{
    if( head == nullptr )
        return false;

    CNode* min = head;
    CNode* min_prev = nullptr;
    CNode* next = min->getSibling();
    CNode* next_prev = min;

    while( next != nullptr )
    {
        if( next->compareTo( min ) < 0 )
        {
            min = next;
            min_prev = next_prev;
        }
        next_prev = next;
        next = next->getSibling();
    }

    removeRoot( min, min_prev );

    id = min->getId();

    min->setParent( nullptr );
    min->setSibling( nullptr );
    min->setChild( nullptr );
    delete min;

    return true;
}
/*--------------------------------------------------------------------------------------------------------------------*/
void CChain::removeRoot( CNode *root, CNode *prev )
{
    if( root == head )
        head = root->getSibling();
    else
        prev->setSibling( root->getSibling() );

    CNode* newhead = nullptr;
    CNode* child = root->getChild();

    while( child != nullptr )
    {
        CNode* next = child->getSibling();
        child->setSibling( newhead );
        child->setParent( nullptr );
        newhead = child;
        child = next;
    }

    CChain* newHeap = new CChain( newhead );

    head = Union( newHeap );

    delete newHeap;

}
/*--------------------------------------------------------------------------------------------------------------------*/
void CChain::BinominalLink( CNode *x, CNode *y )
{
    y->setParent( x );
    y->setSibling( x->getChild() );
    x->setChild( y );
    x->setDegree( x->getDegree() + 1 );
}
/*--------------------------------------------------------------------------------------------------------------------*/
int CChain::compareTo( const CChain *y )
{
    auto x_min = this->findMin();
    auto y_min = y->findMin();

    if( x_min->getRevenue() < y_min->getRevenue() )
        return 1;
    else if( x_min->getRevenue() == y_min->getRevenue() && this->chain_id < y->chain_id )
        return 1;
    else if( x_min->getRevenue() == y_min->getRevenue()
             && this->chain_id == y->chain_id && x_min->getId() < y_min->getId() )
        return 1;
    else if( x_min->getRevenue() == y_min->getRevenue()
             && this->chain_id == y->chain_id && x_min->getId() == y_min->getId() )
        return 0;
    else
        return -1;
}
/*--------------------------------------------------------------------------------------------------------------------*/
int CChain::getMhIndex( ) const { return mh_index; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CChain::setMhIndex( int mhIndex ) { mh_index = mhIndex; }
/*--------------------------------------------------------------------------------------------------------------------*/
int CChain::getChainId() const { return chain_id; }
/*--------------------------------------------------------------------------------------------------------------------*/
void CChain::setChainId(int chainId) { chain_id = chainId; }
/*====================================================================================================================*/
class CHolding
{
private:

    CChain* chains[10000] = { nullptr };

    CChain* chains_by_id[10000] = { nullptr };

    int amount;
public:
    // default constructor
    CHolding        ( );
    // destructor
    ~CHolding       ( );

    int parent      ( int i );

    int left        ( int i );

    int right       ( int i );

    void swap       ( CChain*& x, CChain*& y );

    void bubbleDown ( int i );

    void Add        ( int chain, unsigned int id, unsigned int revenue );

    bool Remove     ( int chain, unsigned int& id );

    bool Remove     ( unsigned int& id );

    void Merge      ( int dstChain, int srcChain );

    bool Merge      ( );

    int getAmount   ( ) const;
};
/*--------------------------------------------------------------------------------------------------------------------*/
CHolding::CHolding( )
{
    amount = 0;
}
/*--------------------------------------------------------------------------------------------------------------------*/
CHolding::~CHolding( )
{
    for( int i = 0; i < amount; i++ )
        if( chains[i] != nullptr )
            delete chains[i];
}
/*--------------------------------------------------------------------------------------------------------------------*/
int CHolding::parent( int i ) { return (i - 1) / 2; }
/*--------------------------------------------------------------------------------------------------------------------*/
int CHolding::left( int i ) { return (2 * i + 1); }
/*--------------------------------------------------------------------------------------------------------------------*/
int CHolding::right( int i ) { return (2* i + 2); }
/*--------------------------------------------------------------------------------------------------------------------*/
void CHolding::swap( CChain *&x, CChain *&y )
{
    CChain* tmp = x;
    x = y;
    y = tmp;
    int tmp_index = x->getMhIndex();
    x->setMhIndex( y->getMhIndex() );
    y->setMhIndex( tmp_index );
}
/*--------------------------------------------------------------------------------------------------------------------*/
void CHolding::Add( int chain, unsigned int id, unsigned int revenue )
{
    int i = 0;

    if( chains_by_id[chain - 1] == nullptr )
    {
        amount++;
        i = amount - 1;
        chains_by_id[chain - 1] = new CChain( );
        chains_by_id[chain - 1]->setChainId( chain );
        chains_by_id[chain - 1]->Insert(id, revenue);
        chains_by_id[chain - 1]->setMhIndex( i );
        chains[i] = chains_by_id[chain - 1];
    }
    else
    {
        i = chains_by_id[chain - 1]->getMhIndex();
        chains_by_id[chain - 1]->Insert(id, revenue);
        chains[i] = chains_by_id[chain - 1];
    }

    while( i != 0 && chains[i]->compareTo(chains[parent(i)]) == 1 )
    {
        swap( chains[parent( i )], chains[i] );
        i = parent( i );
    }
}
/*--------------------------------------------------------------------------------------------------------------------*/
void CHolding::bubbleDown( int i )
{
    int l = left( i );
    int r = right( i );
    int smallest = i;

    if( l < amount && chains[l]->compareTo(chains[i]) == 1 )
        smallest = l;
    if( r < amount && chains[r]->compareTo(chains[smallest]) == 1 )
        smallest = r;

    if( smallest != i )
    {
        swap(chains[i], chains[smallest] );
        bubbleDown( smallest );
    }
}
/*--------------------------------------------------------------------------------------------------------------------*/
bool CHolding::Remove( int chain, unsigned int &id )
{
    int index;
    if( amount <= 0 || chains_by_id[chain - 1] == nullptr )
        return false;

    chains_by_id[chain - 1]->extractMin( id );
    index = chains_by_id[chain - 1]->getMhIndex();

    if( chains[index]->isEmpty() )
    {
        swap( chains[index], chains[amount - 1] );
        delete chains[amount - 1];
        chains[amount - 1] = nullptr;
        chains_by_id[chain - 1] = nullptr;
        amount--;
    }

    bubbleDown( index );

    return true;
}
/*--------------------------------------------------------------------------------------------------------------------*/
bool CHolding::Remove( unsigned int &id )
{
    int index;
    if( amount <= 0 || chains[0] == nullptr )
        return false;

    chains[0]->extractMin( id );

    if( chains[0]->isEmpty() )
    {
        swap( chains[0], chains[amount - 1] );
        index = chains[amount - 1]->getId() - 1;
        delete chains[amount - 1];
        chains[amount - 1] = nullptr;
        chains_by_id[index] = nullptr;
        amount--;
    }

    bubbleDown( 0 );

    return true;
}
/*--------------------------------------------------------------------------------------------------------------------*/
bool CHolding::Merge()
{
    return true;
}
/*--------------------------------------------------------------------------------------------------------------------*/
void CHolding::Merge( int dstChain, int srcChain )
{

}
/*--------------------------------------------------------------------------------------------------------------------*/
int CHolding::getAmount( ) const { return amount; }
/*====================================================================================================================*/

void test01()
{
    unsigned int id;
    CHolding c;
    for( int i = 1; i < 5000; i++ )
        for( int j = 0; j < 500; j++ )
            c.Add( i, j, j);

    while ( c.getAmount() != 0 )
        c.Remove( id );
}

void test02()
{
    bool res;
    unsigned int id;
    //Ukazkovy vstup #1
    //-----------------
    CHolding f1;
    f1 . Add ( 7, 2, 9 );
    f1 . Add ( 12, 4, 4 );
    f1 . Add ( 6, 15, 2 );
    f1 . Add ( 6, 9, 3 );
    res = f1 . Remove ( 12, id );
    // res = true, id = 4
    assert( res && id == 4 );
    id = 0;
    res = f1 . Remove ( 12, id );
    // res = false, id = N/A
    assert( !res && id == 0 );
    id = 0;
    res = f1 . Remove ( 6, id );
    // res = true, id = 15
    assert( res && id == 15 );
    id = 0;
    res = f1 . Remove ( 6, id );
    // res = true, id = 9
    assert( res && id == 9 );
    id = 0;
    res = f1 . Remove ( 6, id );
    // res = false, id = N/A
    assert( !res && id == 0 );

    //Ukazkovy vstup #2
    //-----------------
    CHolding f2;
    f2 . Add ( 4, 2, 2 );
    f2 . Add ( 1, 4, 3 );
    f2 . Add ( 8, 9, 8 );
    id = 0;
    res = f2 . Remove ( id );
    // res = true, id = 2
    assert( res && id == 2 );
    id = 0;
    res = f2 . Remove ( id );
    // res = true, id = 4
    assert( res && id == 4 );
    id = 0;

    //Ukazkovy vstup #3
    //-----------------
    CHolding f3;
    f3 . Add ( 10, 101, 9 );
    f3 . Add ( 10, 102, 8 );
    f3 . Add ( 10, 103, 7 );
    f3 . Add ( 10, 104, 6 );
    f3 . Add ( 10, 105, 5 );
    f3 . Add ( 20, 201, 9 );
    f3 . Add ( 20, 202, 8 );
    f3 . Add ( 20, 203, 7 );
    f3 . Add ( 20, 204, 6 );
    f3 . Add ( 20, 205, 5 );
    f3 . Add ( 30, 301, 9 );
    f3 . Add ( 30, 302, 8 );
    f3 . Add ( 30, 303, 7 );
    f3 . Add ( 30, 304, 6 );
    f3 . Add ( 30, 305, 5 );
    res = f3 . Remove ( id );
    // res = true, id = 105
    assert( res && id == 105 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 205
    assert( res && id == 205 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 305
    assert( res && id == 305 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 104
    assert( res && id == 104 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 204
    assert( res && id == 204 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 304
    assert( res && id == 304 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 103
    assert( res && id == 103 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 203
    assert( res && id == 203 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 303
    assert( res && id == 303 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 102
    assert( res && id == 102 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 202
    assert( res && id == 202 );
    id = 0;
    res = f3 . Remove ( id );
    // res = true, id = 302
    assert( res && id == 302 );
    id = 0;
}

void test03()
{
    CHolding a;
    unsigned id = 666;
    assert( !a . Remove( id ) && id == 666);
    a . Add( 5,5,5);
    a . Add( 6,6,6);
    a . Add( 7,7,7);
    a . Add( 8,8,8);
    assert( a . Remove( 5, id ) && id == 5);
    a . Add(8,2,2);
    assert( a . Remove( id ) && id == 2 );
    a . Add( 1,1,1);
    assert( a . Remove( id ) && id == 1);
}

void test04()
{
    unsigned id;
    CHolding a;
    a . Add(10,10,10); //1
    a . Add(10,10,10);//3
    a . Add(10,10,10);//4
    a . Add(10,10,10);//5
    a . Add(10,10,10);//7
    assert(a . Remove( id ) && id == 10);//1
    a . Add(10,9,10);//2
    assert( a . Remove( id ) && id == 9);//2
    a . Add(10,9,11);//8
    assert( a . Remove( 10, id ) && id == 10);//3
    assert( a . Remove( id ) && id == 10);//4
    a . Add(20, 5, 10);//6
    assert( a . Remove ( id ) && id == 10);//5
    assert( a . Remove( 20, id ) && id == 5);//6
    a . Add( 20,20,20);//11
    assert( a . Remove( id ) && id == 10);//7
    a . Add( 20, 19, 20);//10
    a . Add( 10, 20 , 20);//9
    assert( a . Remove( id ) && id == 9);//8
    assert( a . Remove(10, id ) && id == 20);//9
    assert( a . Remove( id ) && id == 19);//10
    assert( a . Remove( 20, id ) && id == 20);//11
    assert( !a . Remove( id ) && id == 20);//xx
}

void test05()
{
    unsigned id;
    bool res;
    CHolding c3;
    c3.Add(1, 1, 1);
    c3.Add(1, 2, 1);
    c3.Add(1, 3, 2);
    res = c3.Remove(id);
    assert(res && id == 1);
    res = c3.Remove(id);
    assert(res && id == 2);
    res = c3.Remove(id);
    assert(res && id == 3);
    c3.Add(1, 1, 1);
    c3.Add(1, 2, 1);
    c3.Add(1, 3, 2);
    c3.Add(2, 4, 1);
    c3.Add(2, 5, 1);
    c3.Add(2, 6, 2);
    res = c3.Remove(id);
    assert(res && id == 1);
    res = c3.Remove(id);
    assert(res && id == 2);
    res = c3.Remove(id);
    assert(res && id == 4);
    res = c3.Remove(id);
    assert(res && id == 5);
    res = c3.Remove(id);
    assert(res && id == 3);
    res = c3.Remove(id);
    assert(res && id == 6);
}

void test06()
{
    bool res;
    unsigned id;
    CHolding c2;
    c2.Add(30, 80, 4);
    c2.Add(30, 83, 3);
    c2.Add(30, 82, 2);
    c2.Add(30, 81, 1);
    c2.Add(30, 181, 1);
    c2.Add(20, 90, 4);
    c2.Add(20, 93, 3);
    c2.Add(20, 92, 2);
    c2.Add(20, 291, 1);
    c2.Add(20, 191, 1);
//---
    res = c2.Remove(id);
    assert(res && id == 191);
    res = c2.Remove(id);
    assert(res && id == 291);
    res = c2.Remove(id);
    assert(res && id == 81);
    res = c2.Remove(id);
    assert(res && id == 181);
    res = c2.Remove(id);
    assert(res && id == 92);
    res = c2.Remove(id);
    assert(res && id == 82);
    res = c2.Remove(id);
    assert(res && id == 93);
    res = c2.Remove(id);
    assert(res && id == 83);
    res = c2.Remove(id);
    assert(res && id == 90);
    res = c2.Remove(id);
    assert(res && id == 80);
    res = c2.Remove(id);
    assert(!res && id == 80);
}

int main( )
{
    unsigned id;
//    test01();
    test02();
    test03();
    test04();
    test05();
    test06();

    return 0;
}